-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2018 at 11:39 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mynotescode`
--

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nis` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `sustainable` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nis`, `nama`, `jenis_kelamin`, `alamat`, `sustainable`) VALUES
(15, 'HAV 1', 'sustainable 1', 'LRP 1', 'Eco friendly 1', 'Enhancing 1'),
(16, 'HAV 2', 'sustainable 2', 'LRP 2', 'Eco friendly 2', 'Enhancing 2'),
(17, 'HAV 3', 'sustainable 3', 'LRP 3', 'Eco friendly 3', 'Enhancing 3'),
(18, '12', '11', '10', '10', '20'),
(19, '15', '10', '15', '20', '30'),
(20, 'HAV 1', 'sustainable 1', 'LRP 1', 'Eco friendly 1', 'Enhancing 1'),
(21, 'HAV 2', 'sustainable 2', 'LRP 2', 'Eco friendly 2', 'Enhancing 2'),
(22, 'HAV 3', 'sustainable 3', 'LRP 3', 'Eco friendly 3', 'Enhancing 3'),
(23, 'HAV 1', 'sustainable 1', 'LRP 1', 'Eco friendly 1', 'Enhancing 1'),
(24, 'HAV 2', 'sustainable 2', 'LRP 2', 'Eco friendly 2', 'Enhancing 2'),
(25, 'HAV 3', 'sustainable 3', 'LRP 3', 'Eco friendly 3', 'Enhancing 3'),
(26, 'HAV 1', 'sustainable 1', 'LRP 1', 'Eco friendly 1', 'Enhancing 1'),
(27, 'HAV 2', 'sustainable 2', 'LRP 2', 'Eco friendly 2', 'Enhancing 2'),
(28, 'HAV 3', 'sustainable 3', 'LRP 3', 'Eco friendly 3', 'Enhancing 3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
