<?php

class Home extends CI_Controller {
	private $filename = "import_data"; // Kita tentukan nama filenya
	
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('SiswaModel');
	}
	
	function index() {
		$data['siswa'] = $this->SiswaModel->view();
		
		$this->load->view('template/head');
		$this->load->view('home/index', $data);		
		$this->load->view('template/footer');
	}

	function hello() {
		echo "Hello";
	}
	
	
	
}

