<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Endless Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/endless/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Font Awesome -->
	<link href="<?= base_url() ?>assets/endless/css/font-awesome.min.css" rel="stylesheet">

	<!-- Pace -->
	<link href="<?= base_url() ?>assets/endless/css/pace.css" rel="stylesheet">	
	
	<!-- Endless -->
	<link href="<?= base_url() ?>assets/endless/css/endless.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/endless/css/endless-skin.css" rel="stylesheet">

	<link href="<?= base_url() ?>assets/endless/css/bootstrap-wysihtml5.css" rel="stylesheet"/>
	
	    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	
	<!-- Jquery -->
	<script src="<?= base_url() ?>assets/endless/js/jquery-1.10.2.min.js"></script>
	
	<!-- Bootstrap -->
    <script src="<?= base_url() ?>assets/endless/bootstrap/js/bootstrap.min.js"></script>
   
	<!-- Modernizr -->
	<script src='<?= base_url() ?>assets/endless/js/modernizr.min.js'></script>
   
    <!-- Pace -->
	<script src='<?= base_url() ?>assets/endless/js/pace.min.js'></script>
	
	<!-- Popup Overlay -->
	<script src='<?= base_url() ?>assets/endless/js/jquery.popupoverlay.min.js'></script>
   
    <!-- Slimscroll -->
	<script src='<?= base_url() ?>assets/endless/js/jquery.slimscroll.min.js'></script>
    
	<!-- Cookie -->
	<script src='<?= base_url() ?>assets/endless/js/jquery.cookie.min.js'></script>

	<!-- Endless -->
	<script src="<?= base_url() ?>assets/endless/js/endless/endless.js"></script>
	
	<!-- WYSIHTML5 -->
	<script src='<?= base_url() ?>assets/endless/js/wysihtml5-0.3.0.min.js'></script>	
	<script src='<?= base_url() ?>assets/endless/js/uncompressed/bootstrap-wysihtml5.js'></script>	

  </head>

  <body class="overflow-hidden">
	<!-- Overlay Div -->
	<div id="overlay" class="transparent"></div>	
	
	<div id="wrapper" class="preload">
		<div id="top-nav" class="skin-6 fixed">
			<div class="brand">
				<span>SISFORBOYS</span>
				<span class="text-toggle"> Admin</span>
			</div><!-- /brand -->
			<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<ul class="nav-notification clearfix">
				<li class="profile dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<strong>ADMIN</strong>
						<span><i class="fa fa-chevron-down"></i></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="clearfix" href="#">
								<img src="img/user.jpg" alt="User">
								<div class="detail">
									<strong>ADMIN</strong>
									<p class="grey">admin@email.com</p> 
								</div>
							</a>
						</li>
						<li><a tabindex="-1" href="profile.html" class="main-link"><i class="fa fa-edit fa-lg"></i> Edit profile</a></li>
						<li><a tabindex="-1" href="gallery.html" class="main-link"><i class="fa fa-picture-o fa-lg"></i> Photo Gallery</a></li>
						<li><a tabindex="-1" href="#" class="theme-setting"><i class="fa fa-cog fa-lg"></i> Setting</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" class="main-link logoutConfirm_open" href="#logoutConfirm"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /top-nav-->
		
		<aside class="fixed skin-6">	
			<div class="sidebar-inner scrollable-sidebar">
				<div class="size-toggle">
					<a class="btn btn-sm" id="sizeToggle">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
						<i class="fa fa-power-off"></i>
					</a>
				</div><!-- /size-toggle -->	
				<div class="user-block clearfix">
					<img src="img/user.jpg" alt="User">
					<div class="detail">
						<strong>SISFORBOYS</strong><span class="badge badge-danger bounceIn animation-delay4 m-left-xs">4</span>
						<ul class="list-inline">
							<li><a href="profile.html">Profile</a></li>
							<li><a href="inbox.html" class="no-margin">Inbox</a></li>
						</ul>
					</div>
				</div><!-- /user-block -->
				<div class="search-block">
					<div class="input-group">
						<input type="text" class="form-control input-sm" placeholder="search here...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
						</span>
					</div><!-- /input-group -->
				</div><!-- /search-block -->
				<div class="main-menu">
					<ul>
						<li>
							<a href="index.php">
								<span class="menu-icon">
									<i class="fa fa-desktop fa-lg"></i> 
								</span>
								<span class="text">
									Dashboard
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li class="active openable">
							<a href="index.php">
								<span class="menu-icon">
									<i class="fa fa-file-text fa-lg"></i> 
								</span>
								<span class="text">
									Pemilihan Bahan Baku
								</span>
								<span class="menu-hover"></span>
							</a>
							<ul class="submenu">
								<li><a href="index.php">
									<span class="submenu-label">Sign in</span>
								</a></li>
								<li><a href="index.php"><span class="submenu-label">Sign up</span></a></li>
								
							</ul>
						</li>						
						
						<li>
							<a href="index.php/Siswa/form">
								<span class="menu-icon">
									<i class="fa fa-file-text fa-lg"></i> 
								</span>
								<span class="text">
									Pemilihan Lokasi
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
							<a href="index.php">
								<span class="menu-icon">
									<i class="fa fa-file-text fa-lg"></i> 
								</span>
								<span class="text">
									Penentuan Indikator Kinerja
								</span>
								<span class="menu-hover"></span>
							</a>
						</li>
						<li>
							<a href="index.php">
								<span class="menu-icon">
									<i class="fa fa-file-text fa-lg"></i> 
								</span>
								<span class="text">
									Frame Rule
								</span>
								<span class="badge badge-danger bounceIn animation-delay6">4</span>
								<span class="menu-hover"></span>
							</a>
						</li>	

						<li>
							<a href="index.php">
								<span class="menu-icon">
									<i class="fa fa-file-text fa-lg"></i> 
								</span>
								<span class="text">
									Laporan 
								</span>
								<span class="badge badge-danger bounceIn animation-delay6">4</span>
								<span class="menu-hover"></span>
							</a>
						</li>							
					</ul>
					
					<div class="alert alert-info">
						Welcome to SISFORBOYS Group
					</div>
				</div><!-- /main-menu -->
			</div><!-- /sidebar-inner -->
		</aside>

<!-- Content Start -->
